﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PPTBootCamp.Entities.Configurations
{
    public class TutorConfiguration : IEntityTypeConfiguration<Tutor>
    {
        public void Configure(EntityTypeBuilder<Tutor> builder)
        {
            builder.HasKey(tutor => tutor.TutorId);

            builder.Property(tutor => tutor.FirstName)
                .IsRequired();

            builder.Property(tutor => tutor.LastName)
                .IsRequired();

            builder.HasMany(tutor => tutor.Courses)
                .WithOne(course => course.Tutor)
                .HasForeignKey(course => course.TutorId);
        }
    }
}