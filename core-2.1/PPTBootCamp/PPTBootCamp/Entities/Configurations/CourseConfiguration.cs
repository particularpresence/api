﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PPTBootCamp.Entities.Configurations
{
    public class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.HasKey(course => course.CourseId);

            builder.Property(course => course.Name)
                .IsRequired();

            builder.Property(course => course.Description)
                .IsRequired()
                .HasMaxLength(500);
        }
    }
}