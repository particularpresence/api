﻿namespace PPTBootCamp.Models
{
    public class TutorForCreation
    {
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
    }
}