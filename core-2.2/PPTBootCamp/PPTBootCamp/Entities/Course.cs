﻿namespace PPTBootCamp.Entities
{
    public class Course
    {
        public int CourseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DurationInMinutes { get; set; }
        public int TutorId { get; set; }
        public Tutor Tutor { get; set; }    
    }
}